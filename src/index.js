import './index.css'

import Scene from './classes/Scene.class'
import Controls from './classes/Controls.class'

const scene = new Scene(true)
const controls = new Controls()

scene.init()
controls.init(scene)

scene.addPolygon([
  { x: scene.canvas.width / 2, y: scene.canvas.height / 2 },
  { x: scene.canvas.width / 2, y: scene.canvas.height / 2 + 100 },
  { x: scene.canvas.width / 2 - 100, y: scene.canvas.height / 2 + 100 },
  { x: scene.canvas.width / 2 - 100, y: scene.canvas.height / 2 }
])

// scene.addPolygon([
//   { x: scene.canvas.width / 3, y: scene.canvas.height / 3 },
//   { x: scene.canvas.width / 3, y: scene.canvas.height / 3 + 200 },
//   { x: scene.canvas.width / 3 - 100, y: scene.canvas.height / 3 + 200 },
//   { x: scene.canvas.width / 3 - 100, y: scene.canvas.height / 3 }
// ])

scene.addPolygon([
  { x: scene.canvas.width / 3, y: scene.canvas.height / 3 },
  { x: scene.canvas.width / 3 - 50, y: scene.canvas.height / 3 + 200 },
  { x: scene.canvas.width / 3 - 100, y: scene.canvas.height / 3 + 200 },
  { x: scene.canvas.width / 3 - 100, y: scene.canvas.height / 3 }
])

console.log(scene)
