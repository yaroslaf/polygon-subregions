import { CONTROLS, MOUSE } from '../constants'

class Controls {
  init(scene) {
    const { mouse } = scene

    const buttons = document.querySelectorAll('.buttons button')
    const checkboxes = document.querySelectorAll('.switches input')

    buttons.forEach((button, i) => {
      button.onclick = ({ target: { id, classList } }) => {
        mouse.mode = id

        const activeButton = document.querySelector('button.active')

        if (activeButton) {
          activeButton.classList.remove('active')
        }

        classList.add('active')
      }

      // if (button.id === MOUSE.MODE.DRAW_POLYGON) {
      if (button.id === MOUSE.MODE.MOVE_POLYGON) {
        button.click()
      }
    })

    checkboxes.forEach(checkbox => {
      checkbox.onclick = ({ target: { id, checked } }) => {
        switch (id) {
          case CONTROLS.SWITCH.BIND_POINTS_TO_GRID:
            scene.bindMouseCoordsToGrid = checked
            break
          default:
        }
      }

      if (
        scene.bindMouseCoordsToGrid &&
        checkbox.id === CONTROLS.SWITCH.BIND_POINTS_TO_GRID
      ) {
        checkbox.click()
      }
    })
  }
}

export default Controls
