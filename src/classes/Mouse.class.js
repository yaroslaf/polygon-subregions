import { MOUSE } from '../constants'

class Mouse {
  isMouse = true
  mode = null

  x = 0
  y = 0
  r = 0

  prevX = 0
  prevY = 0

  gridX = 0
  gridY = 0

  leftButtonPressed = false
  rightButtonPressed = false

  constructor(scene) {
    const { canvas } = scene
    const header = document.querySelector('header')

    this.setCoords = e => {
      this.setPrevCoords()

      this.x = e.clientX - canvas.offsetLeft
      this.y = e.clientY - canvas.offsetTop - header.clientHeight

      if (!scene.bindMouseCoordsToGrid) {
        return
      }

      const { gridOffset } = scene
      const scaledGridSize = scene.getScaledGridSize()
      const gridX =
        gridOffset.x +
        Math.round((this.x - gridOffset.x) / scaledGridSize) * scaledGridSize
      const gridY =
        gridOffset.y +
        Math.round((this.y - gridOffset.y) / scaledGridSize) * scaledGridSize

      this.gridX = gridX
      this.gridY = gridY
    }

    this.getCoords = () => {
      if (scene.bindMouseCoordsToGrid) {
        return { x: this.gridX, y: this.gridY }
      }

      return { x: this.x, y: this.y }
    }

    this.getDelta = () => {
      const dx = (this.x - this.prevX) / scene.scale
      const dy = (this.y - this.prevY) / scene.scale

      return { dx, dy }
    }

    canvas.oncontextmenu = e => {
      e.preventDefault()
    }

    canvas.onmousemove = e => {
      this.setCoords(e)

      if (this.rightButtonPressed) {
        scene.move()
      }

      const movingPoint = scene.getMovingPoint()
      const movingPolygon = scene.getMovingPolygon()

      if (this.leftButtonPressed) {
        if (scene.isLastPolygonFinished()) {
          if (this.mode === MOUSE.MODE.MOVE_POLYGON_POINT) {
            if (movingPoint) {
              movingPoint.move(scene)
            } else {
              scene.getHoveredPoint()?.move(scene)
            }
          }

          if (this.mode === MOUSE.MODE.MOVE_POLYGON) {
            if (movingPolygon) {
              movingPolygon.move(scene)
            } else {
              scene.getHoveredPolygon()?.move(scene)
            }
          }
        }
      } else {
        if (movingPoint) {
          movingPoint.moving = false
        }

        if (movingPolygon) {
          movingPolygon.moving = false
          movingPolygon.boundData = false
        }
      }
    }

    canvas.onmousedown = e => {
      switch (e.button) {
        case MOUSE.BUTTON.LEFT:
          this.leftButtonPressed = true
          break
        case MOUSE.BUTTON.RIGHT:
          this.rightButtonPressed = true
        default:
      }

      canvas.onmousemove(e)
    }

    canvas.onmouseup = e => {
      switch (e.button) {
        case MOUSE.BUTTON.LEFT:
          this.leftButtonPressed = false

          if (this.mode === MOUSE.MODE.DRAW_POLYGON) {
            scene.addPoint(this)
          }

          break
        case MOUSE.BUTTON.RIGHT:
          this.rightButtonPressed = false
        default:
      }
    }

    canvas.onmousewheel = e => {
      scene.scaling(e)
    }
  }

  setPrevCoords() {
    this.prevX = this.x
    this.prevY = this.y
  }
}

export default Mouse
