import { POLYGON } from '../constants'

class BorderChunk {
  hovered = false

  constructor(points) {
    this.points = points
  }

  render(scene) {
    const { c } = scene

    c.lineWidth = 1

    const color = this.hovered
      ? POLYGON.BORDER.HOVERED_COLOR
      : POLYGON.BORDER.COLOR

    c.fillStyle = color
    c.strokeStyle = color

    c.beginPath()

    this.points.forEach(({ x, y }, i) => {
      const to = i ? c.lineTo : c.moveTo

      to.call(c, scene.toSceneX(x), scene.toSceneY(y))
    })

    c.closePath()
    c.stroke()
    c.fill()
  }
}

export default BorderChunk
