import Stats from 'stats.js'

import Mouse from './Mouse.class'
import Polygon from './Polygon.class'
import Point from './Point.class'

import { SCENE, MOUSE, POINT } from '../constants'

import { polygonPolygonIntersection } from '../helpers/polygon-fns'

class Scene {
  stats = null
  canvas = null
  c = null
  mouse = null

  offset = { x: 0, y: 0 }

  scale = 1

  gridOffset = { x: 0, y: 0 }
  bindMouseCoordsToGrid = false

  polygons = []
  points = []

  constructor(showStats = false) {
    if (showStats) {
      this.stats = new Stats()
      this.stats.dom.className = 'stats'

      this.stats.showPanel(0)
      document.querySelector('main').appendChild(this.stats.dom)
    }
  }

  init() {
    this.canvas = document.querySelector('canvas')
    this.c = this.canvas.getContext('2d')

    this.mouse = new Mouse(this)

    window.onresize = () => {
      this.resizeCanvas()
    }

    window.onresize()

    this.render()
  }

  resizeCanvas() {
    const parent = this.canvas.parentElement

    this.canvas.width = parent.clientWidth
    this.canvas.height = parent.clientHeight
  }

  move() {
    const { mouse } = this
    const { dx, dy } = mouse.getDelta()

    this.offset.x += dx
    this.offset.y += dy
  }

  scaling({ deltaY }) {
    const {
      mouse: { x, y }
    } = this

    const sign = Math.sign(deltaY)
    const scale = Math.exp(-sign * SCENE.SCALE_STEP)
    const newSceneScale = this.scale * scale

    this.offset.x += x / newSceneScale - x / this.scale
    this.offset.y += y / newSceneScale - y / this.scale
    this.scale = newSceneScale
  }

  toSceneX(x) {
    return (this.offset.x + x) * this.scale
  }

  toSceneY(y) {
    return (this.offset.y + y) * this.scale
  }

  toSceneSize(size) {
    return size * this.scale
  }

  toSceneCoords(point) {
    return {
      ...point,
      x: this.toSceneX(point.x),
      y: this.toSceneY(point.y),
      r: this.toSceneSize(point.r)
    }
  }

  fromSceneX(sceneX) {
    return sceneX / this.scale - this.offset.x
  }

  fromSceneY(sceneY) {
    return sceneY / this.scale - this.offset.y
  }

  fromSceneSize(size) {
    return size / this.scale
  }

  fromSceneCoords(point) {
    return {
      ...point,
      x: this.fromSceneX(point.x),
      y: this.fromSceneY(point.y),
      r: this.fromSceneSize(point.r)
    }
  }

  getLastPolygon() {
    return this.polygons.slice(-1)[0]
  }

  getFinishedPolygons() {
    return this.polygons.filter(p => p.isFinished())
  }

  getPolygonsByIds(ids) {
    return this.polygons.filter(({ id }) => ids.includes(id))
  }

  getPointsByIds(ids) {
    return this.points.filter(({ id }) => ids.includes(id))
  }

  addPolygon(points = []) {
    if (points.length) {
      points.forEach(p => this.addPoint(p))

      this.getLastPolygon().close()
    } else {
      const polygon = new Polygon()

      this.polygons.push(polygon)

      return polygon
    }
  }

  isLastPolygonFinished() {
    return this.getLastPolygon()?.isFinished()
  }

  addPoint({ x, y, isMouse }) {
    let addNewPolygon = false
    let lastPolygon = this.getLastPolygon()

    if (lastPolygon) {
      if (lastPolygon.isFinished()) {
        addNewPolygon = true
      } else {
        const points = lastPolygon.getPoints(this)

        if (points[0].hovered) {
          if (points.length >= 3) {
            return lastPolygon.close()
          }

          return
        }
      }
    } else {
      addNewPolygon = true
    }

    const coords = isMouse
      ? this.fromSceneCoords(this.mouse.getCoords())
      : { x, y }
    const point = new Point(coords.x, coords.y)

    this.points.push(point)

    if (addNewPolygon) {
      lastPolygon = this.addPolygon()
    }

    lastPolygon.addPoint(point.id)
  }

  getHoveredPoint() {
    return this.points.find(({ hovered }) => hovered)
  }

  getMovingPoint() {
    return this.points.find(({ moving }) => moving)
  }

  getHoveredPolygon() {
    return this.polygons.find(({ hovered }) => hovered)
  }

  getMovingPolygon() {
    return this.polygons.find(({ moving }) => moving)
  }

  getCursor() {
    return this.canvas.style.cursor
  }

  setCursor(value) {
    if (this.mouse.rightButtonPressed && value !== 'move') {
      return
    }

    this.canvas.style.cursor = value
  }

  gridPointsToArray() {
    return this.gridPoints.reduce((prev, row) => [...prev, ...row], [])
  }

  getScaledGridSize() {
    return this.toSceneSize(SCENE.GRID.SIZE)
  }

  updateGrid() {
    const { offset, scale } = this

    if (scale < 0.3 || scale > 20) {
      return
    }

    const scaledGridSize = this.getScaledGridSize()
    const gridCellsNumberX = Math.abs(offset.x / SCENE.GRID.SIZE)
    const gridCellLeftX =
      (Math.sign(offset.x) === 1
        ? gridCellsNumberX % 1
        : 1 - (gridCellsNumberX % 1)) * scaledGridSize
    const gridCellsNumberY = Math.abs(offset.y / SCENE.GRID.SIZE)
    const gridCellLeftY =
      (Math.sign(offset.y) === 1
        ? gridCellsNumberY % 1
        : 1 - (gridCellsNumberY % 1)) * scaledGridSize

    this.gridOffset = {
      x: gridCellLeftX,
      y: gridCellLeftY
    }
  }

  updatePolygonsOverlaying() {
    const polygons = this.getFinishedPolygons()

    polygons.forEach(p => {
      p.innerPolygonsIds = []
      p.outerPolygonsIds = []
    })

    if (polygons.length < 2) {
      return
    }

    for (let i = 0; i < polygons.length; i++) {
      const polygon1 = polygons[i]

      for (let j = 0; j < polygons.length; j++) {
        const polygon2 = polygons[j]

        if (polygon1.id === polygon2.id) {
          continue
        }

        if (
          polygonPolygonIntersection(
            polygon2.getPoints(this),
            polygon1.getPoints(this)
          ) === 2
        ) {
          polygon1.innerPolygonsIds.push(polygon2.id)
          polygon2.outerPolygonsIds.push(polygon1.id)
        }
      }
    }
  }

  update() {
    const { mouse, points, polygons } = this

    this.updateGrid()
    this.updatePolygonsOverlaying()

    let unsetCursorPointer = true

    polygons.forEach(p => {
      p.update(this)

      if (unsetCursorPointer) {
        const { borderChunks } = p

        for (let i = 0; i < borderChunks.length; i++) {
          if (borderChunks[i].hovered) {
            unsetCursorPointer = false
            break
          }
        }
      }
    })

    points.forEach(p => {
      p.update(this)

      if (unsetCursorPointer && p.hovered) {
        unsetCursorPointer = false
      }
    })

    if (mouse.rightButtonPressed) {
      this.setCursor('move')
    } else {
      if (
        unsetCursorPointer ||
        (mouse.mode === MOUSE.MODE.DRAW_POLYGON && this.isLastPolygonFinished())
      ) {
        this.setCursor('default')
      }
    }
  }

  clear() {
    this.c.clearRect(0, 0, this.canvas.width, this.canvas.height)
  }

  renderGrid() {
    const { canvas, c, scale, gridOffset } = this

    if (scale < 0.3 || scale > 20) {
      return
    }

    const scaledGridSize = this.getScaledGridSize()

    c.strokeStyle = SCENE.GRID.COLOR
    c.lineWidth = this.toSceneSize(SCENE.GRID.LINE_WIDTH)

    for (let x = gridOffset.x; x < canvas.width; x += scaledGridSize) {
      c.beginPath()
      c.moveTo(x, 0)
      c.lineTo(x, canvas.height)
      c.stroke()
      c.closePath()
    }

    for (let y = gridOffset.y; y < canvas.height; y += scaledGridSize) {
      c.beginPath()
      c.moveTo(0, y)
      c.lineTo(canvas.width, y)
      c.stroke()
      c.closePath()
    }
  }

  renderPointer() {
    const { c, mouse } = this

    if (mouse.mode !== MOUSE.MODE.DRAW_POLYGON) {
      return
    }

    const { x, y } = this.bindMouseCoordsToGrid
      ? {
          x: mouse.gridX,
          y: mouse.gridY
        }
      : mouse

    c.fillStyle = POINT.COLOR

    c.beginPath()
    c.arc(
      this.toSceneX(this.fromSceneX(x)),
      this.toSceneY(this.fromSceneY(y)),
      3,
      0,
      Math.PI * 2
    )
    c.closePath()
    c.fill()
  }

  render() {
    const { stats, c, polygons } = this

    if (stats) {
      stats.begin()
    }

    c.textAlign = 'center'
    c.textBaseline = 'middle'

    this.clear()
    this.update()
    this.renderGrid()

    // сначала закрашиваем полигоны
    polygons.forEach(p => p.fill(this))

    // потом рисуем границы полигонов, чтобы они были сверху закрашиваемой области (для вложенных полигонов)
    polygons.forEach(p => p.render(this))

    this.renderPointer()

    if (stats) {
      stats.end()
    }

    requestAnimationFrame(() => this.render())
  }
}

export default Scene
