import { v4 } from 'uuid'

import { POLYGON, MOUSE, SCENE } from '../constants'

import {
  getAngleBetweenPoints,
  getDistanceBetweenPoints,
  negativeAngleToPositive,
  roundToDecimals
} from '../helpers/helpers'
import { lineLineIntersection } from '../helpers/intersection'
import {
  isPointInsidePolygon,
  sortPointsInClockwiseOrder,
  sortPointsInCounterClockwiseOrder,
  getPolygonCenter,
  getPolygonWithInnerPolygonsArea,
  createBorderChunks,
  rotatePolygonAroundPoint,
  getSidesFromPoints
} from '../helpers/polygon-fns'

import BorderChunk from './BorderChunk.class'

class Polygon {
  status = POLYGON.STATUS.IN_PROCESS

  pointsIds = []
  borderChunks = []
  innerPolygonsIds = []
  outerPolygonsIds = []

  center = { x: null, y: null }
  area = null

  hovered = false
  moving = false

  boundData = null

  constructor() {
    this.id = v4()
    this.halfBorderWidth = POLYGON.BORDER.WIDTH / 2
  }

  isInProcess() {
    return this.status === POLYGON.STATUS.IN_PROCESS
  }

  isFinished() {
    return this.status === POLYGON.STATUS.FINISHED
  }

  addPoint(pointId) {
    this.pointsIds.push(pointId)
  }

  close() {
    this.status = POLYGON.STATUS.FINISHED
  }

  getPoints(scene, withMouse = false) {
    const points = scene.getPointsByIds(this.pointsIds)

    if (withMouse && this.isInProcess()) {
      points.push(scene.fromSceneCoords(scene.mouse.getCoords()))
    }

    return points
  }

  getInnerPolygons(scene) {
    if (this.isInProcess()) {
      return []
    }

    return scene.getPolygonsByIds(this.innerPolygonsIds)
  }

  getInnerPolygonsPoints(scene, onlyFirst = false) {
    let innerPolygons = this.getInnerPolygons(scene)

    if (onlyFirst && innerPolygons.length) {
      const minNestingLevel = Math.min(
        ...innerPolygons.map(({ outerPolygonsIds }) => outerPolygonsIds.length)
      )

      innerPolygons = innerPolygons.filter(
        ({ outerPolygonsIds }) => outerPolygonsIds.length === minNestingLevel
      )
    }

    return innerPolygons.map(innerPolygon => innerPolygon.getPoints(scene))
  }

  getSides(scene) {
    return getSidesFromPoints(this.getPoints(scene))
  }

  move(scene) {
    const { mouse } = scene
    let { dx, dy } = mouse.getDelta()

    this.moving = true

    if (this.boundData) {
      const { polygonSide, movingPolygonSide } = this.boundData

      const boundSideAngle = getAngleBetweenPoints(
        polygonSide.p2,
        polygonSide.p1,
        true
      )
      const deltaAngle = negativeAngleToPositive(Math.atan2(dy, dx))
      const moveAngle =
        Math.max(boundSideAngle, deltaAngle) -
          Math.min(boundSideAngle, deltaAngle) >
        Math.PI / 2
          ? boundSideAngle + Math.PI
          : boundSideAngle

      // TODO: работает, но лучше отрефакторить...

      const distances = [
        { p1: polygonSide.p1, p2: movingPolygonSide.p1 },
        { p1: polygonSide.p1, p2: movingPolygonSide.p2 },
        { p1: polygonSide.p2, p2: movingPolygonSide.p1 },
        { p1: polygonSide.p2, p2: movingPolygonSide.p2 }
      ].reduce((distances, line) => {
        return [
          ...distances,
          { ...line, distance: getDistanceBetweenPoints(line.p1, line.p2) }
        ]
      }, [])
      const maxDistance = Math.max(...distances.map(({ distance }) => distance))
      const mouseDeltaLength = Math.hypot(dx, dy)

      dx = mouseDeltaLength * Math.cos(moveAngle)
      dy = mouseDeltaLength * Math.sin(moveAngle)

      const maxDistanceData = distances.find(
        ({ distance }) => distance === maxDistance
      )
      const newPointCoords = {
        x: maxDistanceData.p2.x + dx,
        y: maxDistanceData.p2.y + dy
      }
      const lengthLimit =
        getDistanceBetweenPoints(polygonSide.p1, polygonSide.p2) +
        getDistanceBetweenPoints(movingPolygonSide.p1, movingPolygonSide.p2)

      if (
        getDistanceBetweenPoints(newPointCoords, maxDistanceData.p1) >
        lengthLimit
      ) {
        return
      }
    }

    this.getPoints(scene).forEach(point => point.move(scene, false, { dx, dy }))
  }

  rotate(scene, rotationPoint, rotationAngle) {
    const points = this.getPoints(scene)
    const rotatedPoints = rotatePolygonAroundPoint(
      points,
      rotationPoint,
      rotationAngle
    )

    points.forEach((point, pointIndex) => {
      point.x = rotatedPoints[pointIndex].x
      point.y = rotatedPoints[pointIndex].y
    })
  }

  bindToOtherPolygonSide(scene) {
    if (!this.moving || this.boundData) {
      return
    }

    // const { c } = scene

    const movingPolygonSides = this.getSides(scene)
    const otherPolygons = scene.getPolygonsByIds(
      scene.polygons.map(p => p.id).filter(id => id !== this.id)
    )

    for (let i = 0; i < otherPolygons.length; i++) {
      const polygon = otherPolygons[i]
      const polygonSides = polygon.getSides(scene)
      const candidates = []

      for (let j = 0; j < polygonSides.length; j++) {
        const polygonSide = polygonSides[j]

        movingPolygonSides.forEach(movingPolygonSide => {
          const intersectionPoint = lineLineIntersection(
            movingPolygonSide,
            polygonSide
          )

          if (intersectionPoint) {
            // c.fillStyle = 'red'
            // c.beginPath()
            // c.arc(
            //   scene.toSceneX(intersectionPoint.x),
            //   scene.toSceneY(intersectionPoint.y),
            //   5,
            //   0,
            //   Math.PI * 2
            // )
            // c.closePath()
            // c.fill()

            const polygonSideAngle = getAngleBetweenPoints(
              polygonSide.p2,
              polygonSide.p1,
              true
            )
            const movingPolygonSideAngle = getAngleBetweenPoints(
              movingPolygonSide.p2,
              movingPolygonSide.p1,
              true
            )

            let rotationAngle =
              movingPolygonSideAngle > polygonSideAngle
                ? movingPolygonSideAngle - polygonSideAngle
                : -(polygonSideAngle - movingPolygonSideAngle)

            // TODO: все таки не получится всегда брать просто меньший угол, нужен ТОЛЬКО ВНЕШНИЙ угол

            if (Math.abs(rotationAngle) > Math.PI / 2) {
              rotationAngle =
                (Math.PI - Math.abs(rotationAngle)) * Math.sign(rotationAngle)
            }

            candidates.push({
              point: intersectionPoint,
              angle: rotationAngle,
              polygonSide,
              movingPolygonSide
            })
          }
        })
      }

      if (candidates.length === 2) {
        const roundedCandidateAngles = candidates.map(candidate => {
          return Math.round((Math.abs(candidate.angle) * 180) / Math.PI)
        })

        const areCandidatesHaveSameAngle =
          roundedCandidateAngles[0] === roundedCandidateAngles[1]

        if (areCandidatesHaveSameAngle) {
          // TODO: привязать к лучшей стороне...
        } else {
          const rotationCandidate = candidates.reduce(
            (candidateWithMinAngle, candidate) => {
              if (
                Math.abs(candidate.angle) <
                Math.abs(candidateWithMinAngle.angle)
              ) {
                return candidate
              }

              return candidateWithMinAngle
            },
            candidates[0]
          )
          const { point, angle, polygonSide, movingPolygonSide } =
            rotationCandidate

          // console.log((angle * 180) / Math.PI)

          this.rotate(scene, point, angle)

          this.boundData = {
            polygonSide,
            movingPolygonSide
          }
        }

        break
      }
    }
  }

  updateBorder(scene) {
    const points = this.getPoints(scene, true)

    if (points.length < 2) {
      return
    }

    this.borderChunks = createBorderChunks(
      points,
      POLYGON.BORDER.WIDTH,
      this.isFinished(),
      POLYGON.BORDER.MAX_ANGLE_DIFFERENCE
    ).map(points => new BorderChunk(points))
  }

  updateBorderIntersection(scene) {
    const mouse = scene.fromSceneCoords(scene.mouse)

    if (mouse.mode !== MOUSE.MODE.HOVER_POLYGON_BORDER_CHUNKS) {
      return
    }

    this.borderChunks.forEach(chunk => {
      chunk.hovered = isPointInsidePolygon(mouse, chunk.points)

      if (chunk.hovered && scene.getCursor() !== 'pointer') {
        scene.setCursor('pointer')
      }
    })
  }

  updateCenter(scene) {
    if (this.isInProcess()) {
      return
    }

    this.center = getPolygonCenter(
      this.getPoints(scene),
      this.getInnerPolygonsPoints(scene, true)
    )
  }

  updateArea(scene) {
    if (this.isInProcess()) {
      return
    }

    this.area =
      getPolygonWithInnerPolygonsArea(
        this.getPoints(scene),
        this.getInnerPolygonsPoints(scene)
      ) *
      (SCENE.METERS_PER_PIXEL * SCENE.METERS_PER_PIXEL)
  }

  update(scene) {
    this.updateBorder(scene)
    this.updateBorderIntersection(scene)
    this.updateCenter(scene)
    this.updateArea(scene)
    // this.bindToOtherPolygonSide(scene)

    const mouse = scene.fromSceneCoords(scene.mouse)

    if (isPointInsidePolygon(mouse, this.getPoints(scene))) {
      this.hovered = true
    } else {
      this.hovered = false
    }
  }

  fill(scene) {
    if (this.isInProcess() || this.outerPolygonsIds.length) {
      return
    }

    const { c } = scene
    const pointsInClockwiseOrder = sortPointsInClockwiseOrder(
      this.getPoints(scene)
    )

    c.beginPath()

    pointsInClockwiseOrder.forEach(({ x, y }, pi) => {
      const to = pi === 0 ? c.moveTo : c.lineTo

      to.call(c, scene.toSceneX(x), scene.toSceneY(y))
    })

    c.closePath()

    const innerPolygons = this.getInnerPolygons(scene)

    innerPolygons.forEach(polygon => {
      const innerPointsInCounterClockwiseOrder =
        sortPointsInCounterClockwiseOrder(polygon.getPoints(scene))

      innerPointsInCounterClockwiseOrder.forEach(({ x, y }, pi) => {
        const to = pi === 0 ? c.moveTo : c.lineTo

        to.call(c, scene.toSceneX(x), scene.toSceneY(y))
      })

      c.closePath()
    })

    c.fillStyle = POLYGON.FILL_COLOR

    if (scene.mouse.mode === MOUSE.MODE.MOVE_POLYGON) {
      const movingPolygon = scene.getMovingPolygon()

      if (movingPolygon) {
        if (this.id === movingPolygon.id) {
          c.fillStyle = POLYGON.HOVERED_FILL_COLOR
        }
      } else if (this.hovered) {
        c.fillStyle = POLYGON.HOVERED_FILL_COLOR
      }
    }

    c.fill()
  }

  renderBorder(scene) {
    this.borderChunks.forEach(chunk => chunk.render(scene))
  }

  renderArea(scene) {
    const { area } = this

    if (area === null) {
      return
    }

    const { c } = scene

    c.fillStyle = SCENE.TEXT_COLOR

    c.fillText(
      `${area.toFixed(2)} м2`,
      scene.toSceneX(this.center.x),
      scene.toSceneY(this.center.y)
    )
  }

  render(scene) {
    const { c, mouse } = scene
    const points = this.getPoints(scene)

    this.renderBorder(scene)
    this.renderArea(scene)
    this.bindToOtherPolygonSide(scene)

    points.forEach((point, pointIndex) => {
      const { x, y } = point

      c.fillStyle = 'black'
      c.font = '10px sans-serif'

      // c.fillText(
      //   `${x.toFixed(2)}, ${y.toFixed(2)}`,
      //   scene.toSceneX(x),
      //   scene.toSceneY(y)
      // )

      c.fillText(pointIndex, scene.toSceneX(x), scene.toSceneY(y))
    })

    if (this.isFinished()) {
      if (mouse.mode === MOUSE.MODE.MOVE_POLYGON_POINT) {
        const movingPoint = scene.getMovingPoint()

        points.forEach(point => {
          let renderPoint = false

          if (movingPoint) {
            if (point.id === movingPoint.id) {
              renderPoint = true
            }
          } else if (point.hovered) {
            renderPoint = true
          }

          if (!renderPoint) {
            return
          }

          point.render(scene)
          scene.setCursor('pointer')
        })
      } else if (mouse.mode === MOUSE.MODE.MOVE_POLYGON && this.hovered) {
        scene.setCursor('pointer')
      }
    }

    if (this.isInProcess()) {
      points[0].render(scene)

      if (
        mouse.mode === MOUSE.MODE.DRAW_POLYGON &&
        points.length > 2 &&
        points[0].hovered
      ) {
        scene.setCursor('pointer')
      }
    }
  }
}

export default Polygon
