import { v4 } from 'uuid'

import { POINT, POLYGON } from '../constants'

import { circleCircleIntersection } from '../helpers/intersection'

class Point {
  hovered = false
  moving = false
  r = POLYGON.BORDER.WIDTH

  constructor(x, y) {
    this.id = v4()
    this.x = x
    this.y = y
  }

  move(scene, bindMouseCoordsToGrid, customDelta) {
    const { mouse } = scene

    this.moving = true

    if (bindMouseCoordsToGrid === undefined) {
      bindMouseCoordsToGrid = scene.bindMouseCoordsToGrid
    }

    if (bindMouseCoordsToGrid) {
      this.x = scene.fromSceneX(mouse.gridX)
      this.y = scene.fromSceneY(mouse.gridY)
    } else {
      const { dx, dy } = customDelta || mouse.getDelta()

      this.x += dx
      this.y += dy
    }
  }

  update(scene) {
    const point = scene.toSceneCoords(this)

    if (circleCircleIntersection(point, scene.mouse)) {
      this.hovered = true
    } else {
      this.hovered = false
    }
  }

  render(scene) {
    const { c } = scene
    const { x, y, r } = this

    c.fillStyle = POINT.COLOR
    c.strokeStyle = POINT.STROKE_COLOR
    c.lineWidth = 1

    c.beginPath()
    c.arc(
      scene.toSceneX(x),
      scene.toSceneY(y),
      scene.toSceneSize(r),
      0,
      Math.PI * 2
    )
    c.closePath()
    c.fill()
    c.stroke()
  }
}

export default Point
