export const copyPoints = points => {
  return points.map(({ x, y }) => ({ x, y }))
}

export const getDistanceBetweenPoints = (p1, p2) => {
  const dx = p1.x - p2.x
  const dy = p1.y - p2.y

  return Math.hypot(dx, dy)
}

export const negativeAngleToPositive = angle => {
  if (angle < 0) {
    return angle + Math.PI * 2
  }

  return angle
}

export const getAngleBetweenPoints = (p1, p2, onlyPositive = false) => {
  const angle = Math.atan2(p1.y - p2.y, p1.x - p2.x)

  if (onlyPositive) {
    return negativeAngleToPositive(angle)
  }

  return angle
}

export const roundToDecimals = (value, accuracy = 2) => {
  return Number(value.toFixed(accuracy))
}
