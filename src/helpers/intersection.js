import { getDistanceBetweenPoints } from './helpers'

export const circleCircleIntersection = (c1, c2) => {
  return getDistanceBetweenPoints(c1, c2) <= c1.r + c2.r
}

export const lineLineIntersection = (l1, l2) => {
  const x1 = l1.p1.x
  const y1 = l1.p1.y
  const x2 = l1.p2.x
  const y2 = l1.p2.y

  const x3 = l2.p1.x
  const y3 = l2.p1.y
  const x4 = l2.p2.x
  const y4 = l2.p2.y

  const uA =
    ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) /
    ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1))
  const uB =
    ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) /
    ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1))

  if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
    return {
      x: x1 + uA * (x2 - x1),
      y: y1 + uA * (y2 - y1)
    }
  }

  return false
}
