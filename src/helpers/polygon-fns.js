import robustPointInPolygon from 'robust-point-in-polygon'
import polylabel from 'polylabel'

import { copyPoints, getAngleBetweenPoints } from './helpers'
import { lineLineIntersection } from './intersection'

export const transformPointsToMatrix = points => {
  return points.map(({ x, y }) => [x, y])
}

export const getSidesFromPoints = points => {
  return points.reduce((sides, point, pointIndex) => {
    return [
      ...sides,
      {
        p1: point,
        p2: points[pointIndex + 1] || points[0]
      }
    ]
  }, [])
}

export const isPointInsidePolygon = (point, polygonPoints) => {
  const result = robustPointInPolygon(
    transformPointsToMatrix(polygonPoints),
    transformPointsToMatrix([point])[0]
  )

  if (result === -1) {
    return 2 // point is inside polygon
  }

  if (result === 0) {
    return 1 // point is on the side of polygon
  }

  return false
}

export const polygonPolygonIntersection = (polygon1, polygon2) => {
  const lines1 = getSidesFromPoints(polygon1)
  const lines2 = getSidesFromPoints(polygon2)

  for (let i = 0; i < lines1.length; i++) {
    for (let j = 0; j < lines2.length; j++) {
      if (lineLineIntersection(lines1[i], lines2[j])) {
        return 1
      }
    }
  }

  if (isPointInsidePolygon(polygon1[0], polygon2)) {
    // polygon1 is inside polygon2
    return 2
  }

  return false
}

export const arePointsInClockwiseOrder = points => {
  const tmpPoints = [...points, points.slice(0, 1)[0]]
  let sum = 0

  for (let i = 0; i < tmpPoints.length - 1; i++) {
    const p1 = tmpPoints[i]
    const p2 = tmpPoints[i + 1]

    sum += (p2.x - p1.x) * (p2.y + p1.y)
  }

  return sum < 0
}

export const sortPointsInClockwiseOrder = points => {
  const newPoints = copyPoints(points)
  const isClockwiseOrder = arePointsInClockwiseOrder(newPoints)

  if (!isClockwiseOrder) {
    return newPoints.reverse()
  }

  return newPoints
}

export const sortPointsInCounterClockwiseOrder = points => {
  const newPoints = copyPoints(points)
  const isClockwiseOrder = arePointsInClockwiseOrder(newPoints)

  if (isClockwiseOrder) {
    return newPoints.reverse()
  }

  return newPoints
}

export const transformPointsToGeoJSONFormat = (
  polygonPoints,
  innerPolygonsPoints
) => {
  return [
    transformPointsToMatrix(polygonPoints),
    ...innerPolygonsPoints.map(polygonPoints =>
      transformPointsToMatrix(polygonPoints)
    )
  ]
}

export const getPolygonCenter = (polygonPoints, innerPolygonsPoints) => {
  const [x, y] = polylabel(
    transformPointsToGeoJSONFormat(polygonPoints, innerPolygonsPoints)
  )

  return { x, y }
}

export const getPolygonArea = polygonPoints => {
  const sums = polygonPoints.reduce((prev, p1, i) => {
    const p2 = polygonPoints[i + 1] || polygonPoints[0]

    return prev + p1.x * p2.y
  }, 0)
  const diffs = polygonPoints.reduce((prev, p1, i) => {
    const p2 = polygonPoints[i + 1] || polygonPoints[0]

    return prev - p1.y * p2.x
  }, 0)

  return Math.abs(sums + diffs) / 2
}

export const getPolygonWithInnerPolygonsArea = (
  polygonPoints,
  innerPolygonsPoints
) => {
  const polygonArea = getPolygonArea(polygonPoints)
  const innerPolygonsAreas = innerPolygonsPoints.map(innerPolygonPoints =>
    getPolygonArea(innerPolygonPoints)
  )

  return innerPolygonsAreas.reduce((polygonArea, innerPolygonArea) => {
    return polygonArea - innerPolygonArea
  }, polygonArea)
}

/**
 * Создает границу полигона из трапециевидных чанков
 * @param {{ x: number, y: number }[]} polygonPoints - Массив точек полигона
 * @param {number} [borderWidth] - Ширина границы
 * @param {boolean} [isClosed] - Полигон замкнут?
 * @param {number} [angleToCutCorner] - Угол в радианах между сторонами полигона, при котором обрезается внешний угол
 * @returns {{ x: number, y: number }[][]} Двумерный массив чанков
 */
export const createBorderChunks = (
  polygonPoints,
  borderWidth = 10,
  isClosed = true,
  angleToCutCorner = (8 * Math.PI) / 180
) => {
  const halfBorderWidth = borderWidth / 2
  const corners = {
    inner: [],
    outer: []
  }
  const borderChunks = []

  const getDirection = key => (key === 'inner' ? -1 : 1)

  const addCorner = (
    key,
    point,
    angle,
    changeOrder = false,
    length = halfBorderWidth
  ) => {
    const { x, y } = point

    corners[key].push({
      coords: {
        ...point,
        x: x - Math.cos(angle) * length,
        y: y - Math.sin(angle) * length
      },
      changeOrder
    })
  }

  polygonPoints.forEach((p, i) => {
    const prevPoint = polygonPoints[i - 1] || polygonPoints.slice(-1)[0]
    const nextPoint = polygonPoints[i + 1] || polygonPoints[0]
    const angleBetweenCurrentAndPrev = getAngleBetweenPoints(p, prevPoint)
    const angleBetweenCurrentAndNext = getAngleBetweenPoints(p, nextPoint)

    if (!isClosed && i === 0) {
      Object.keys(corners).forEach(key => {
        const rightAngleBetweenCurrentAndNext =
          angleBetweenCurrentAndNext - (Math.PI / 2) * getDirection(key)

        addCorner(key, p, rightAngleBetweenCurrentAndNext)
      })
    } else if (!isClosed && i === polygonPoints.length - 1) {
      Object.keys(corners).forEach(key => {
        const rightAngleBetweenCurrentAndPrev =
          angleBetweenCurrentAndPrev + (Math.PI / 2) * getDirection(key)

        addCorner(key, p, rightAngleBetweenCurrentAndPrev)
      })
    } else {
      const diffBetweenNextAndPrev =
        angleBetweenCurrentAndNext - angleBetweenCurrentAndPrev
      const halfAngleBetweenNextAndPrev =
        angleBetweenCurrentAndPrev +
        diffBetweenNextAndPrev / 2 +
        (Math.abs(diffBetweenNextAndPrev) > Math.PI ? Math.PI : 0)
      const oppositeHalfAngleBetweenNextAndPrev =
        halfAngleBetweenNextAndPrev + Math.PI

      Object.keys(corners).forEach(key => {
        const rightAngleBetweenCurrentAndPrev =
          angleBetweenCurrentAndPrev + (Math.PI / 2) * getDirection(key)
        const angleBetweenOppositeHalfAndRight =
          oppositeHalfAngleBetweenNextAndPrev - rightAngleBetweenCurrentAndPrev
        const isAngleLowerThanAngleToCutCorner =
          Math.abs(angleBetweenOppositeHalfAndRight - Math.PI / 2) <
            angleToCutCorner ||
          Math.abs(angleBetweenOppositeHalfAndRight - (Math.PI / 2 + Math.PI)) <
            angleToCutCorner

        if (isAngleLowerThanAngleToCutCorner) {
          const angle =
            key === 'inner'
              ? angleBetweenCurrentAndNext - Math.PI / 2
              : rightAngleBetweenCurrentAndPrev

          addCorner(key, p, angle, true)
        } else {
          const cornerLength =
            halfBorderWidth / Math.cos(angleBetweenOppositeHalfAndRight)

          addCorner(
            key,
            p,
            oppositeHalfAngleBetweenNextAndPrev,
            false,
            cornerLength
          )
        }
      })
    }
  })

  const cornersLength = corners.inner.length + (isClosed ? 0 : -1)

  for (let i = 0; i < cornersLength; i++) {
    let points = [
      corners.inner[i].coords,
      (corners.inner[i + 1] || corners.inner[0]).coords,
      (corners.outer[i + 1] || corners.outer[0]).coords,
      corners.outer[i].coords
    ]

    if (corners.inner[i].changeOrder) {
      points = [points[2], points[0], points[3], points[1]]
    }

    borderChunks.push(points)
  }

  return borderChunks
}

export const rotatePolygonAroundPoint = (
  polygonPoints,
  rotationPoint,
  rotationAngle
) => {
  const rotatedPoints = []

  for (let i = 0, l = polygonPoints.length; i < l; i++) {
    const p = polygonPoints[i]
    const tmpX = p.x - rotationPoint.x
    const tmpY = p.y - rotationPoint.y

    const rotatedX =
      tmpX * Math.cos(rotationAngle) - tmpY * Math.sin(rotationAngle)
    const rotatedY =
      tmpX * Math.sin(rotationAngle) + tmpY * Math.cos(rotationAngle)

    rotatedPoints[i] = {
      ...p,
      x: rotatedX + rotationPoint.x,
      y: rotatedY + rotationPoint.y
    }
  }

  return rotatedPoints
}

export const getCentroid = points => {
  const [sumX, sumY] = points.reduce(
    ([x, y], point) => {
      return [x + point.x, y + point.y]
    },
    [0, 0]
  )

  return {
    x: sumX / points.length,
    y: sumY / points.length
  }
}
