export const CENTIMETERS_PER_PIXEL = 1
export const METERS_PER_PIXEL = CENTIMETERS_PER_PIXEL / 100
export const GRID = {
  LINE_WIDTH: 1,
  SIZE: CENTIMETERS_PER_PIXEL * 25,
  COLOR: '#eceff1'
}
export const TEXT_COLOR = '#1565c0'
export const TEXT_SIZE = 12
export const SCALE_STEP = 0.2

export const DECIMALS_ACCURACY = 2
