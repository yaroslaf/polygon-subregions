export const STATUS = {
  IN_PROCESS: 'IN_PROCESS',
  FINISHED: 'FINISHED'
}
export const BORDER = {
  COLOR: '#42a5f5',
  HOVERED_COLOR: '#1e88e5',
  WIDTH: 10,
  MAX_ANGLE_DIFFERENCE: (8 * Math.PI) / 180
}
export const FILL_COLOR = '#e3f2fd'
export const HOVERED_FILL_COLOR = '#ffebee'
