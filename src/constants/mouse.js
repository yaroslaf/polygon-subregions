export const MODE = {
  DRAW_POLYGON: 'DRAW_POLYGON',
  MOVE_POLYGON_POINT: 'MOVE_POLYGON_POINT',
  HOVER_POLYGON_BORDER_CHUNKS: 'HOVER_POLYGON_BORDER_CHUNKS',
  MOVE_POLYGON: 'MOVE_POLYGON'
}
export const BUTTON = {
  LEFT: 0,
  RIGHT: 2
}
