## Main files are in /src folder

### How to try it

1. `npm i` - install node modules
2. `npm run build` - to generate minified bundle and index.html file
3. Open `dist/index.html` in browser
4. `npm run start` - to serve project locally in development mode

#### TODOS:

- Переписать на React
- Добавление точки на стороне полигона - деление стороны на две части
- Подписи размеров сторон полигона

### Helpful links

- https://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
- https://codepen.io/loclegkxim/pen/eNeeZQ/
- https://github.com/mikolalysenko/robust-point-in-polygon

- https://github.com/mapbox/polylabel
- https://github.com/mapbox/earcut
- https://github.com/brendankenny/libtess.js
- https://github.com/r3mi/poly2tri.js
